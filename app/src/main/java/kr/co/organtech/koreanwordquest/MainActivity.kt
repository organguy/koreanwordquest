package kr.co.organtech.koreanwordquest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar!!.hide()
    }

    fun initView(){

    }

    fun gotoStep(view: View) {

        var tags = view.tag.toString().split("_")

        if(tags[0] == "c"){
            var intent = Intent(this, CharActivity::class.java)
            var step = tags[1].toInt()
            intent.putExtra("STEP", step)
            startActivity(intent)
        }else{
            var intent = Intent(this, WordActivity::class.java)
            var step = tags[1].toInt()
            intent.putExtra("STEP", step)
            startActivity(intent)
        }

    }
}