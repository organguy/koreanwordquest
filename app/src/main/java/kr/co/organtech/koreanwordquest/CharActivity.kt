package kr.co.organtech.koreanwordquest

import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.github.kimkevin.hangulparser.HangulParser
import kotlinx.android.synthetic.main.activity_quest.*
import java.util.*
import kotlin.collections.ArrayList

class CharActivity : AppCompatActivity() {

    var step = 0

    var wordList = ArrayList<WordData>()
    var wordData : WordData? = null
    var tts : TextToSpeech? = null
    var count = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quest)

        initData()
        initView()
        initTTS()

        supportActionBar!!.hide()
    }

    private fun initData(){

        step = intent.getIntExtra("STEP", 0);

        var wordArr: Array<String>? = null

        if(step >= 1){
            wordArr = resources.getStringArray(R.array.char_list_step_1)
        }

        if(step >= 2){
            wordArr = wordArr!!.plus(resources.getStringArray(R.array.char_list_step_2))
        }

        if(step >= 3){
            wordArr = wordArr!!.plus(resources.getStringArray(R.array.char_list_step_3))
        }

        if(step >= 4){
            wordArr = wordArr!!.plus(resources.getStringArray(R.array.char_list_step_4))
        }

        if(step >= 5){
            wordArr = wordArr!!.plus(resources.getStringArray(R.array.char_list_step_5))
        }

        if(step >= 6){
            wordArr = wordArr!!.plus(resources.getStringArray(R.array.char_list_step_6))
        }

        for(word in wordArr!!){
            var wordData = WordData()
            wordData.word = word

            wordList.add(wordData)
        }

        loggingNewWord(wordArr)
    }

    private fun loggingNewWord(wordArr: Array<String>) {
        var filterChar = ""

        if (step == 1) {
            filterChar = "ㅏ"
        } else if (step == 2) {
            filterChar = "ㅓ"
        } else if (step == 3) {
            filterChar = "ㅗ"
        } else if (step == 4) {
            filterChar = "ㅜ"
        } else if (step == 5) {
            filterChar = "ㅡ"
        } else if (step == 6) {
            filterChar = "ㅣ"
        }


        for (word1 in wordArr) {
            for (word2 in wordArr) {
                var jsonList = HangulParser.disassemble(word1 + word2)
                for (json in jsonList) {
                    if (json.equals(filterChar)) {
                        Log.d("word_text", word1 + word2)
                        break
                    }
                }
            }
        }
    }

    private fun initView(){
        tvTitle.text = String.format("%d단계", step)
        setRandomWord()
    }

    private fun initTTS() {
        tts = TextToSpeech(this) {
            tts!!.language = Locale.KOREAN
            tts!!.setPitch(2.0f)
            tts!!.setSpeechRate(0.5f)
        }
    }

    private fun setRandomWord(){
        val random = Random()
        var randomNum = random.nextInt(wordList.size)

        wordData = wordList[randomNum]

        if(wordData!!.isCorrect){
            setRandomWord()
        }else{
            tvWord.text = wordData!!.word
            wordData!!.isCorrect = true

            var solvedCount = String.format("%d / %d", ++count, wordList.size)
            btNext.text = solvedCount
        }
    }

    fun onClickBack(view: View) {
        finish()
    }

    fun onClickSound(view: View) {
        tts!!.speak(wordData!!.word, TextToSpeech.QUEUE_FLUSH, null, null)
    }

    fun onClickNext(view: View) {
        if(count == (wordList.size)){
            Toast.makeText(this, "모든 문제를 풀었습니다.", Toast.LENGTH_SHORT).show()
        }else{
            setRandomWord()
        }
    }
}